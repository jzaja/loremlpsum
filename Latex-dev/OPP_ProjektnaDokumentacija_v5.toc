\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{4}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{9}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{11}{subsection.3.1.1}% 
\contentsline {subsubsection}{Opis obrazaca uporabe}{11}{subsubsection*.2}% 
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{18}{subsubsection*.3}% 
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{20}{subsection.3.1.2}% 
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{24}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{25}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Baza podataka}{26}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{26}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{30}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Dijagram razreda}{31}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Dijagram stanja}{37}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{39}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{40}{section.4.5}% 
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{41}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{41}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{42}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{42}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Ispitivanje sustava}{46}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{52}{section.5.3}% 
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{53}{section.5.4}% 
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{57}{chapter.6}% 
\contentsline {chapter}{Popis literature}{58}{chapter*.4}% 
\contentsline {chapter}{Indeks slika i dijagrama}{60}{chapter*.5}% 
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{61}{chapter*.6}% 
