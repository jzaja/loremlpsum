import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Router } from '@angular/router';

import { Mechanic } from '../Entity/Mechanic';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-mechanics',
  templateUrl: './mechanics.component.html',
  styleUrls: ['./mechanics.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class MechanicsComponent implements OnInit {
  dataSource = ELEMENT_DATA0;
  columnsToDisplay = ['pin', 'firstName', 'lastName'];
  expandedElement: Mechanic | null;
  admin: any = {};

  constructor(private _router: Router,private _userService: UserService) { }
  
  ngOnInit() {
    this._userService.getAdminByEmail(res => {
      this.admin = {
        firstName: res.givenName,
        lastName: res.familyName,
        pin: res.oib,
        role: res.Role,
        email: res.email
      }
    })
  }

  Promijeni(mechanic:Mechanic){
    console.log(mechanic);
  }
  SwitchScreen(){
    this._router.navigate(['/MyView/parts']);
  }
  Logout(){
    console.log("Admin Logging out");
    this._router.navigate(['/home']);
    localStorage.removeItem("token");
  }
  Ukloni(mechanic: Mechanic){
    console.log("deleting mechanic");
  }
}

const ELEMENT_DATA0: Mechanic[] = [
  {
    pin: '1',
    firstName: 'Bruno',
    lastName: 'Jeric',
    carServicePin: '11234234',
    role:0,
    password:"asfasdf",
    email:"b@b.com"
  },{
    pin: '2',
    firstName: 'Ante',
    lastName: 'Pusic',
    carServicePin: '11234234',
    role:0,
    password:"asfasdf",
    email:"b@b.com"
  },{
    pin: '3',
    firstName: 'Bruno',
    lastName: 'Skendrovic',
    carServicePin: '11234234',
    role:0,
    password:"asfasdf",
    email:"b@b.com"
  }
];
