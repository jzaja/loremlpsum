import { Component, OnInit } from '@angular/core';
import { AuthServiceService} from '../Services/auth-service.service';
import {User} from '../Entity/User';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import { CarServiceService } from '../Services/car-service.service';

@Component({
  selector: 'app-register-carservice',
  templateUrl: './register-carservice.component.html',
  styleUrls: ['./register-carservice.component.css']
})
export class RegisterCarserviceComponent implements OnInit {
  registerData:any = {};
 
  constructor(private _carService:CarServiceService,
    private _router: Router) { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  
  ngOnInit() {
  }

  RegisterService() {
    this._router.navigate(['register-mechanic']);//temp
    this._carService.RegisterCarService(this.registerData,res=>{
      this._router.navigate(['register-mechanic']);
    })
  }
  Done(){
    console.log("service registered");
    this._router.navigate(['/register-part']);
  }
}