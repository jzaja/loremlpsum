import { Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Router } from '@angular/router';

import {WorkOrder} from '../Entity/WorkOrder';
import { CarPart } from '../Entity/CarPart';
import { Recomandation } from '../Entity/recomandation';
import {Mechanic} from '../Entity/Mechanic';
import { UserService } from '../Services/user.service';

//promini
@Component({
  selector: 'app-mechanic-profile',
  templateUrl: './mechanic-profile.component.html',
  styleUrls: ['./mechanic-profile.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class MechanicProfileComponent implements OnInit {
  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['workOrderCode', 'registrationPlate', 'listOfParts', 'mileage', 'carStatus'];
  expandedElement: WorkOrder | null;

  mechanic: any = {};
  //mechanic = new Mechanic("1","2345","Ante", "Pusic", "qwerty", "t@t.com");

  constructor(private _router: Router, private _userService: UserService) { }

  ngOnInit() {
    this._userService.getMechanicByEmail(res => {
      this.mechanic = {
        firstName: res.givenName,
        lastName: res.familyName,
        pin: res.oib,
        role: res.Role,
        email: res.email
      }
    })
  }

  Preuzmi(workOrder:WorkOrder){
    if(workOrder.carStatus<1){
      console.log("Preuzeo: " + workOrder);
      workOrder.carStatus += 1;
    } else {
      console.log("Nalog odrađen");
      workOrder.carStatus = 2;
      const index: number = ELEMENT_DATA.indexOf(workOrder);
      if(index!=-1)ELEMENT_DATA.splice(index,1)
      console.log(this.dataSource);
    }
  }
  Logout(){
    console.log("Mechanic Logging out");
    this._router.navigate(['/home/register']);
    this._router.navigate(['/home']);
    localStorage.removeItem('token');
  }
}

const carPart: CarPart = new CarPart('s','s');
const recomandation: Recomandation = new Recomandation('everything okay');

var ELEMENT_DATA: WorkOrder[] = [
  {
    workOrderCode: '0',
    registrationPlate: 'ST-3889-H',
    listOfParts: [carPart,carPart],
    mileage:  190000,
    mechanicPin: '24234234234',
    carStatus: 0,
    recomandation: recomandation
  },{
    workOrderCode: '1',
    registrationPlate: 'ST-222-H',
    listOfParts: [carPart,carPart],
    mileage:  210000,
    mechanicPin: '234234234',
    carStatus: 0,
    recomandation: recomandation
  },{
    workOrderCode: '2',
    registrationPlate: 'ST-444-M',
    listOfParts: [carPart,carPart],
    mileage:  250000,
    mechanicPin: '4353454353',
    carStatus: 0,
    recomandation: recomandation
  }
];
