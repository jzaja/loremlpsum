import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Router } from '@angular/router';

import { CarPart } from '../Entity/CarPart';
import { Mechanic } from '../Entity/Mechanic';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})

export class PartsComponent implements OnInit {
    dataSource = ELEMENT_DATA0;
    columnsToDisplay = ['partCode', 'partName'];
    expandedElement: Mechanic | CarPart | null;
    admin:any={};
    constructor(private _router: Router,private _userService: UserService) { }
    
    ngOnInit() {
      this._userService.getAdminByEmail(res => {
        this.admin = {
          firstName: res.givenName,
          lastName: res.familyName,
          pin: res.oib,
          role: res.Role,
          email: res.email
        }
      })
    }
  
    Promijeni(carPart:CarPart){
      console.log(carPart);
    }
    SwitchScreen(){
      this._router.navigate(['/MyView/mechanics']);
    }
    Logout(){
      console.log("Admin Logging out");
      this._router.navigate(['/home']);
      localStorage.removeItem("token");
    }
    Ukloni(carPart: CarPart){
      console.log("deleting carpart");
    }
  }
  
  const ELEMENT_DATA0: CarPart[] = [
    {
      partCode: '1',
      partName: 'hauba'
    },{
      partCode: '2',
      partName: 'mijenjač'
    },{
      partCode: '3',
      partName: 'stragaglednik'
    }
  ];