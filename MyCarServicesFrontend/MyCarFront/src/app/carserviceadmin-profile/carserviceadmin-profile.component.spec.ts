import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarserviceadminProfileComponent } from './carserviceadmin-profile.component';

describe('CarserviceadminProfileComponent', () => {
  let component: CarserviceadminProfileComponent;
  let fixture: ComponentFixture<CarserviceadminProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarserviceadminProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarserviceadminProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
