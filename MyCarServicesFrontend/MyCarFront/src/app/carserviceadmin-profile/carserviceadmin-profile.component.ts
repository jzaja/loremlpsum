import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CarPart } from '../Entity/CarPart';
import { Mechanic } from '../Entity/Mechanic';

@Component({
  selector: 'app-carserviceadmin-profile',
  templateUrl: './carserviceadmin-profile.component.html',
  styleUrls: ['./carserviceadmin-profile.component.css']
})
export class CarserviceadminProfileComponent implements OnInit {
  
  constructor(private _router: Router) { }
  
  ngOnInit() {
    console.log("loading");
  }

  Promijeni(carPart:CarPart){
    console.log(carPart);
  }
  Logout(){
    localStorage.removeItem("token");
    console.log("Admin Logging out");
    this._router.navigate(['/home/register']);
  }
}