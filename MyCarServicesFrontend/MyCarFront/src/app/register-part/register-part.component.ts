import { Component, OnInit } from '@angular/core';
import { AuthServiceService} from '../Services/auth-service.service';
import {User} from '../Entity/User';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-register-part',
  templateUrl: './register-part.component.html',
  styleUrls: ['./register-part.component.css']
})
export class RegisterPartComponent implements OnInit {
  registerUserData = {};
 
  constructor(private _auth: AuthServiceService,
    private _router: Router) { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  
  ngOnInit() {
  }

  RegisterPart() {
    console.log(this.registerUserData);
    /*
    this._auth.registerUser(this.registerUserData)
    .subscribe(
      res => {
        localStorage.setItem('token', res.token)
        this._router.navigate(['/MyView'])
      },
      err => console.log(err)
    )*/
  }
  Done(){
    this._router.navigate(['/register-mechanic']);
  }
}