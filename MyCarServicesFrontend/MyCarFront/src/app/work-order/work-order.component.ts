import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {CarService} from '../Entity/CarService';
import {CarPart} from '../Entity/CarPart';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './work-order.component.html',
  styleUrls: ['./work-order.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class WorkOrderComponent implements OnInit {
  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['carServicePin', 'carServiceName', 'regularServiceCost'];
  expandedElement: CarService | null;
  
  checked = [];

  constructor(private _router: Router) {}

  ngOnInit() {
  }

  Odaberi(car:CarService){
    console.log(this.checked);
    this._router.navigate(['/MyView/user']);
  }
  Logout(){
    console.log("Workorder Logging out");
    this._router.navigate(['/home/register']);
    localStorage.removeItem("token");
  }
}

const carPart1: CarPart = new CarPart('s','guma');
const carPart2: CarPart = new CarPart('x','volan');

const ELEMENT_DATA: CarService[] = [
  { carServicePin:"3242349",
    carServiceName:"autoCisla",
    adress:"moja 1",
    regularServiceCost:100,
    partsInStock: [carPart1, carPart2]
  },{
    carServicePin:"1234234",
    carServiceName:"autoSkender",
    adress:"moja 2",
    regularServiceCost:250,
    partsInStock: [carPart1, carPart2]
  },{
    carServicePin:"234234324",
    carServiceName:"autoIvana",
    adress:"moja 3",
    regularServiceCost:500,
    partsInStock: [carPart1, carPart2]
  }
];


