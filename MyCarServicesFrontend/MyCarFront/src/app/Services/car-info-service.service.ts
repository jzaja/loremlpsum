import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Car } from '../Entity/Car';
import { callbackify } from 'util';

@Injectable({
  providedIn: 'root'
})
export class CarInfoServiceService {

  constructor(private http: HttpClient) { }

  private baseUrl = "http://localhost:8080";
  private _tickets = this.baseUrl + '/ticket';//+registracija
  private _cars = this.baseUrl + '/vehicles/all';//+user oib
  private _car = this.baseUrl + '/vehicles/';
  /* registrationPlate:string;
      manufacturer:string;
      oib
      hasFault:boolean;
      nextServiceDate:Date;*/
  getOwnerCars(oib, callback) {
    this.http.get(this._cars + oib).subscribe((cars) => {
      callback(cars);
    })
  }

  getCarByPlate(reg: string, callback) {
    console.log("šaljen zahtjev");
    this.http.get(this._car).subscribe(res => {
      callback(res);
    })
  }

  postOwnerCar(car: any, callback) {
    console.log(car);
    this.http.post(this._car, {
      "licensePlate": car.licensePlate,
      "carBrand": car.carBrand,
      "oib": car.oib,
      "hasMalfunction": car.hasFault,
      "serviceDate": car.nextServiceDate
    }).subscribe((res) => {
      //console.log(res);
      callback(res);
    },
      (error) => {
        callback(error.error.licensePlate);
      }
    )
  }
  /*
  {
	"licensePlate":"ST-410-I",
	"carBrand":"Grc2",
	"oib":"12345678912",
	"hasMalfunction":"0",
	"serviceDate":"2020-01-08T16:39:22.539+0000"
}*/

  GetAllUserWorkOrders() {
  }
}
