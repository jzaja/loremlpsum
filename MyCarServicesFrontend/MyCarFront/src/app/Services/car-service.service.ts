import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class CarServiceService {

  constructor(private _http: HttpClient, private _auth: AuthServiceService) { }
  private baseUrl = "http://localhost:8080"
  private _registerCarServiceUrl = this.baseUrl + '/carservice';

  RegisterCarService(data: any, callback) {
    var admin = JSON.parse(localStorage.getItem('admin'));
    localStorage.removeItem('admin');
    admin.CarServicePin = data.CarServicePin;

    this._auth.adminRegistration({
      "givenName": admin.firstName,
      "familyName": admin.lastName,
      "email": admin.email,
      "oib": admin.pin,
      "oibOfService": admin.CarServicePin,
      "password": admin.password
    }, res => {
      var hash = btoa(admin.email + ":" + admin.password);
      
      localStorage.setItem('token',hash);
      localStorage.setItem('oibOfService', data.CarServicePin);
      console.log(data.CarServicePin);
      this._http.post(this._registerCarServiceUrl,
        {
          "addressOfService": data.CarServiceAddress,
          "nameOfService": data.CarServiceName,
          "price": data.RegularServiceCost,
          "oibOfService": data.CarServicePin
        }).subscribe(res => callback(res));
      });
    

  }
}
