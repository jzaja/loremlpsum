import { Injectable } from '@angular/core';
import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthServiceService } from './auth-service.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (localStorage.getItem('token') == null) {
      return next.handle(
        req.clone({
        })
      );
    } else {
      return next.handle(
        req.clone({
          headers: req.headers.append('Authorization', 'Basic ' + localStorage.getItem('token'))
        })
      );
    }
  }
}
