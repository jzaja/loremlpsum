import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  private baseUrl = "http://localhost:8080";
  private userUrl = this.baseUrl + "/proprietor/info";
  private mechanicUrl = this.baseUrl + "/mechanic/info";
  private adminUrl = this.baseUrl + "/administrator/info";

  getUserByEmail(callback) {
    this.http.get(this.userUrl).subscribe(res => {
      console.log("callback")
      console.log(res)
      callback(res)
      
    })
    return;
  }
  getMechanicByEmail(callback) {
    this.http.get(this.mechanicUrl).subscribe(res => {
      console.log("callback")
      console.log(res)
      callback(res)
      
    })
    return;
  }
  getAdminByEmail(callback) {
    this.http.get(this.adminUrl).subscribe(res => {
      console.log("callback")
      console.log(res)
      callback(res)
      
    })
    return;
  }
}
