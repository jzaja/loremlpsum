import { TestBed } from '@angular/core/testing';

import { CarInfoServiceService } from './car-info-service.service';

describe('CarInfoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarInfoServiceService = TestBed.get(CarInfoServiceService);
    expect(service).toBeTruthy();
  });
});
