import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'
import { callbackify } from 'util';

@Injectable()
export class AuthServiceService {
  private baseUrl = "http://localhost:8080"
  private _registerUserUrl = this.baseUrl + '/proprietor';
  private _registerAdminUrl = this.baseUrl + '/administrator';
  private _registerMechanicUrl = this.baseUrl + '/mechanic';
  private _loginUrl = this.baseUrl + '/login';
  private _putMechanicUrl = this.baseUrl + '/carservice/';

  constructor(private http: HttpClient,
    private _router: Router) { }

  adminRegistration(user, callback) {
    console.log(user);
    return this.http.post<any>(this._registerAdminUrl, user).subscribe(res => callback(res));
  }
  registerUser(user, callback) {
    return this.http.post<any>(this._registerUserUrl, user).subscribe(res => callback(res));
  }
  registerMechanic(user, callback) {
    var oibOfServiceTemp = localStorage.getItem('oibOfService');
    localStorage.removeItem('oibOfService');
    user.oibOfService = oibOfServiceTemp;
    console.log(this._putMechanicUrl + oibOfServiceTemp + '/mechanics/' + user.email);
    return this.http.post<any>(this._registerMechanicUrl, user).subscribe(
      res =>
        this.http.put(this._putMechanicUrl + oibOfServiceTemp + '/mechanics/' + user.email, {}).subscribe(res => callback(res)));
  }
  loginUser(user, callback) {
    return this.http.post<any>(this._loginUrl, { "email": String(user.email), "pass": String(user.pass) }).subscribe((res) => callback(res),
      (error) => {
        callback(error.error);
      })
  }

  logoutUser() {
    localStorage.removeItem('token')
    //this._router.navigate(['/home'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }
}
