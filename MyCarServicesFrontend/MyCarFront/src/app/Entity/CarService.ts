import { CarPart } from './CarPart';

export class CarService{
    carServicePin:string;
    carServiceName:string;
    adress:string;
    regularServiceCost:number;
    partsInStock:CarPart[];
    constructor(carServicePin:string,
        carServiceName:string,
        adress:string,
        partsInStock:CarPart[],
        regularServiceCost:number){
            this.carServicePin=carServicePin;
            this.carServiceName=carServiceName;
            this.adress=adress;
            this.partsInStock=partsInStock;
            this.regularServiceCost=regularServiceCost;
        }
}