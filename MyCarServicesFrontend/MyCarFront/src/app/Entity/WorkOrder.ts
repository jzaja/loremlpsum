import { CarPart } from './CarPart';
import { Recomandation } from './recomandation';

export class WorkOrder{
    workOrderCode:string;
    registrationPlate:string;
    mechanicPin:string;
    listOfParts:CarPart[];
    carStatus:number;//enum
    mileage:number;
    recomandation:Recomandation
    constructor(workOrderCode:string,
        registrationPlate:string,
        mechanicPin:string,
        listOfParts:CarPart[],
        carStatus:number,
        recomandation:Recomandation,
        mileage:number){
            this.workOrderCode=workOrderCode;
            this.registrationPlate=registrationPlate;
            this.mechanicPin=mechanicPin;
            this.listOfParts=listOfParts;
            this.carStatus=carStatus;
            this.recomandation=recomandation;
            this.mileage=mileage;
        }
}