export class CarPart{
    partCode:string;
    partName:string;
    constructor(partCode:string,
        partName:string){
        this.partCode=partCode;
        this.partName=partName;
    }

}