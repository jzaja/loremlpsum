export class Car{
    registrationPlate:string;
    manufacturer:string;
    pin:string;
    hasFault:number;
    nextServiceDate:Date;
   
}