import { Role } from "../Entity/RoleEnum";
export class User{
    firstName:string;
    lastName:string;
    pin: string;
    role:Role;
    password: string;
    email: string;
   
    constructor(
        firstName: string,
        lastName: string,
        pin:string,
        password: string,
        email: string
    ){
        this.firstName=firstName;
        this.lastName=lastName;
        this.role=Role.CarOwner;
        this.password=password;
        this.pin=pin;
        this.email=email;
    }
}