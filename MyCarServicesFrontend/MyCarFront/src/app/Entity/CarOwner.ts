import { User } from './User';
export class CarOwner extends User{

    /*
    firstName: string,
        lastName: string,
        pin:string,
        password: string,
        email: string
    */
   
    constructor(servicePin:string,
        firstName: string,
        lastName: string,
        email: string,
        pin:string,
        password:string
        // Password: string,
        ){
        super(firstName,lastName,pin,password,email);
    }
}