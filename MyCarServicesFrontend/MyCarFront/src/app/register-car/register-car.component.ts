import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../Services/auth-service.service';
import { Car } from '../Entity/Car';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { CarInfoServiceService } from '../Services/car-info-service.service';

@Component({
  selector: 'app-register-car',
  templateUrl: './register-car.component.html',
  styleUrls: ['./register-car.component.css']
})

export class RegisterCarComponent implements OnInit {
  registerCarData: any={}; 
  constructor(private _auth: AuthServiceService,
    private _router: Router,private _route: ActivatedRoute, private _carInfoServices: CarInfoServiceService) { }


  ngOnInit() {
    this._route.paramMap.subscribe(par=>{
      const reg=par.get('registrationPlate');
      this.getCarData(reg);
    })
  }
  private getCarData(reg:string){
    let car;
    if(reg=="0"){
      console.log("nula je")

      this.registerCarData={};
    }else{
      console.log("nije nula")
      this._carInfoServices.getCarByPlate(reg,(res)=>{
        car=res;
        console.log(car)
        this.registerCarData.registrationPlate=res.licensePlate;
        this.registerCarData.carBrand=res.carBrand;
        this.registerCarData.oib=res.oib;
      });
    }
  }
  /*{
    "licensePlate": "ST-960-I",
    "carBrand": "Lada",
    "oib": "12345678912",
    "hasMalfunction": 0,
    "serviceDate": "2021-01-11T12:54:38.252+0000"
} */
  Done(){
    this._router.navigate(['/MyView/user']);
  }

  RegisterCar() {
    this.registerCarData.hasFault=0;
    var oneYearFromNow = new Date();
    oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() + 1);
    this.registerCarData.nextServiceDate=oneYearFromNow;

    this._carInfoServices.postOwnerCar(this.registerCarData,(res)=>{
      console.log(res)
      alert(res);
    });
  }
}

const car: Car = {
  registrationPlate: "ST-000-A",
  manufacturer: "Renault",
  pin: "12345678912",
  hasFault: 0,
  nextServiceDate: new Date("2019-02-16")
}