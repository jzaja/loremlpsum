import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../Services/auth-service.service';
import { User } from '../Entity/User';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  constructor(private _auth: AuthServiceService,
    private _router: Router, private _http: HttpClient) { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  registerUserData: any = {};
  ngOnInit() {
  }

  testAdmin() {
    console.log("Logging admin")
    this._router.navigate(['/register-carservice']);
  }
  /*
  {
	"givenName":"Bruno",
	"familyName":"Jeric",
	"email":"b@b.com",
	"oib":"12345678912",
	"password":"123456"
}
  */
  RegisterAsUser() {
    var hash = btoa(this.registerUserData['email'] + ":" + this.registerUserData['password']);
    this._auth.registerUser({
      "givenName": this.registerUserData.firstName,
      "familyName": this.registerUserData.lastName,
      "email": this.registerUserData.email,
      "oib": this.registerUserData.pin,
      "password": this.registerUserData.password
    }, res => {
      localStorage.setItem('token', hash);
      this._router.navigate(['/MyView/user']);
    });

  }

  RegisterAsAdmin() {
    var hash = btoa(this.registerUserData['email'] + ":" + this.registerUserData['password']);
    localStorage.setItem("admin", JSON.stringify(this.registerUserData));
    this._router.navigate(['/register-carservice']);
    /*this._auth.registerAdmin({"givenName":this.registerUserData.firstName,
                              "familyName":this.registerUserData.lastName,
                              "email":this.registerUserData.email,
                              "oib":this.registerUserData.pin,
                              "password":this.registerUserData.password
                            }, res => {
                              localStorage.setItem('token', hash);
                              this._router.navigate(['/MyView/user']);
                            });*/
  }
}