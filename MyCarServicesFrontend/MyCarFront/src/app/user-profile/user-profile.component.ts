import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { Car } from '../Entity/Car';
import { User } from '../Entity/User';
import { CarInfoServiceService } from '../Services/car-info-service.service';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class UserProfileComponent implements OnInit {
  dataSource = ELEMENT_DATA;
  columnsToDisplay = ['licensePlate', 'carBrand', 'oib', 'hasMalfunction', 'serviceDate'];
  expandedElement: Car | null;

  user: any = {};
  //user = new User("Toma", "Sikora", "1234", "qwerty", "t@t.com");

  constructor(private _router: Router, private _carInfoServices: CarInfoServiceService, private _userService: UserService) {
  }
  /*
    firstName:string;
    lastName:string;
    pin: string;
    role:Role;
    password: string;
    email: string;
  */
  ngOnInit() {
    this._userService.getUserByEmail(res => {
      this.user = {
        firstName: res.givenName,
        lastName: res.familyName,
        pin: res.oib,
        role: res.Role,
        email: res.email
      }
      //ELEMENT_DATA=res.vehicles
      this._carInfoServices.getOwnerCars(this.user.pin,(cars) => {
        ELEMENT_DATA = cars;
        this.dataSource = ELEMENT_DATA;
      });
    })
    
  }

  NoviNalog(car: Car) {
    console.log(car);
    
    this._router.navigate(['/WorkOrder']);
  }
  test(){
    console.log(this.user)
  }
  Logout() {
    console.log("User Logging out");
    this._router.navigate(['/home/register']);
    localStorage.removeItem('token');
  }
  AddCar() {
    this._router.navigate(['/car-detail/0']);
  }
  Ukloni(car: Car) {
    console.log("deleting car");
  }
  Uredi(car: any) {
    console.log(car.licensePlate);
    this._router.navigate(['/car-detail', car.licensePlate]);


  }
}
let ELEMENT_DATA: Car[];
/*
const ELEMENT_DATA: Car[] = [
  {
    manufacturer: 'Renault',
    hasFault:false,
    registrationPlate: 'ST-3889-H',
    nextServiceDate:  new Date("2019-02-16")
  },{
    manufacturer: 'Lada',
    hasFault:true,
    registrationPlate: 'ST-222-H',
    nextServiceDate:  new Date("2019-02-16")
  },{
    manufacturer: 'BMW',
    hasFault:false,
    registrationPlate: 'ST-444-M',
    nextServiceDate:  new Date("2019-02-16")
  }
];*/
