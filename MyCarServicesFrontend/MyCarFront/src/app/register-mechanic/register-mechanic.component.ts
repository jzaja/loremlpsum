import { Component, OnInit } from '@angular/core';
import { AuthServiceService} from '../Services/auth-service.service';
import {User} from '../Entity/User';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-register-mechanic',
  templateUrl: './register-mechanic.component.html',
  styleUrls: ['./register-mechanic.component.css']
})
export class RegisterMechanicComponent implements OnInit {
  registerUserData:any = {};
 
  constructor(private _auth: AuthServiceService,
    private _router: Router) { }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  
  ngOnInit() {
  }

  /* RegisterAsUser() {
    var hash = btoa(this.registerUserData['email'] + ":" + this.registerUserData['password']);
    this._auth.registerUser({
      "givenName": this.registerUserData.firstName,
      "familyName": this.registerUserData.lastName,
      "email": this.registerUserData.email,
      "oib": this.registerUserData.pin,
      "password": this.registerUserData.password
    }, res => {
      localStorage.setItem('token', hash);
      this._router.navigate(['/MyView/user']);
    });

  } */

  RegisterMechanic() {  
    this._auth.registerMechanic({"givenName":this.registerUserData.firstName,
                              "familyName":this.registerUserData.lastName,
                              "email":this.registerUserData.email,
                              "oibOfService":this.registerUserData.oibOfService,
                              "oib":this.registerUserData.pin,
                              "password":this.registerUserData.password
                            }, res => {
                            });
  }
  Done(){
    this._router.navigate(['/MyView/mechanics']);
  }
}