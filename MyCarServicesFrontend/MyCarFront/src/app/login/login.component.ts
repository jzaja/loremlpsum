import { Component, OnInit } from '@angular/core';
import { HttpService } from '../Services/http.service';
import { Router } from '@angular/router';
import { LoginModel } from '../Entity/LoginModel';
import { FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../Services/auth-service.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginUserData: any = {};
  constructor(private _auth: AuthServiceService,
    private _router: Router) { }
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  ngOnInit() {
  }
  LoginUserClick() {
    console.log(this.loginUserData)
    this._auth.loginUser({ "email": this.loginUserData.email, "pass": this.loginUserData.password },res=>{
      console.log(res)
      localStorage.setItem('token',res.token);
      if(res.role=="admin"){
        this._router.navigate(['/MyView/mechanics']);
      }
      else if(res.role=="user"){
        this._router.navigate(['/MyView/user']);
        
      }else if(res.role=="mechanic"){
        this._router.navigate(['/MyView/mechanic']);
      }else{
        alert(res.error);
      }
    });
  }
}
