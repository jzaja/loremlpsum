import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { HomeComponent} from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import {AuthServiceService} from '../app/Services/auth-service.service';
import { MatIconModule } from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './Services/token-interceptor.service';
import { MechanicProfileComponent } from './mechanic-profile/mechanic-profile.component';
import {CarserviceadminProfileComponent} from './carserviceadmin-profile/carserviceadmin-profile.component';
import { WorkOrderComponent } from './work-order/work-order.component';
import {MatButtonModule} from '@angular/material';

import { RegisterCarserviceComponent } from './register-carservice/register-carservice.component';
import { PartsComponent } from './parts/parts.component';
import { MechanicsComponent } from './mechanics/mechanics.component';
import { RegisterMechanicComponent } from './register-mechanic/register-mechanic.component';
import { RegisterPartComponent } from './register-part/register-part.component';
import { RegisterCarComponent } from './register-car/register-car.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserProfileComponent,
    HomeComponent,
    MechanicProfileComponent,
    CarserviceadminProfileComponent,
    WorkOrderComponent,
    RegisterCarserviceComponent,
    PartsComponent,
    MechanicsComponent,
    RegisterMechanicComponent,
    RegisterPartComponent,
    RegisterCarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    MatTableModule,
    MatCheckboxModule,
    MatButtonModule
  ],
  providers: [AuthServiceService,{
    provide: HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi:true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
