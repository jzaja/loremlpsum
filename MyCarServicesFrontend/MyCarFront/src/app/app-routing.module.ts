import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent} from './login/login.component';
import { RegisterComponent} from './register/register.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {MechanicProfileComponent} from './mechanic-profile/mechanic-profile.component';
import { CarserviceadminProfileComponent} from './carserviceadmin-profile/carserviceadmin-profile.component';
import { WorkOrderComponent} from './work-order/work-order.component'
import {HomeComponent} from './home/home.component';
import { RegisterCarserviceComponent} from './register-carservice/register-carservice.component'
import { AuthGuard } from './auth.guard';
import { PartsComponent} from './parts/parts.component'
import { MechanicsComponent } from './mechanics/mechanics.component'
import { RegisterMechanicComponent } from './register-mechanic/register-mechanic.component'
import {RegisterPartComponent} from './register-part/register-part.component'
import {RegisterCarComponent} from './register-car/register-car.component'


const routes: Routes = [
  { 
    path: 'home',
    component: HomeComponent,
    children:[
      {path:'register', component:RegisterComponent},
      {path: 'login', component: LoginComponent}
    ]
  },
  {path:'register-part', component:RegisterPartComponent},
  {path:'register-mechanic', component:RegisterMechanicComponent},
  {path:'register-carservice', component:RegisterCarserviceComponent},
  {path:'register-car',component:RegisterCarComponent},
  {path:'car-detail/:{registrationPlate}', component:RegisterCarComponent},
  { path: 'MyView',
    children:[
      {path:'user', component:UserProfileComponent},
      {path: 'mechanic', component: MechanicProfileComponent},
      {path:'mechanics', component: MechanicsComponent},
      {path:'parts', component: PartsComponent}
    ] },
  {path:'WorkOrder', component:WorkOrderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
