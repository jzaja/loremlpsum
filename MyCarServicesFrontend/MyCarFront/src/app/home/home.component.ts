import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { element } from 'protractor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  constructor(private _router:Router) {
     
   }

  ngOnInit() {
    localStorage.removeItem("token");
    window.onload = function() {
      document.getElementById('fakeBody').className +='loaded';
    };
  }
  Switch(){
    console.log("ok")
    var loginEl = document.getElementById("login");
    var registerEl = document.getElementById("register");

    
    if(registerEl.classList.contains('selected')){
      registerEl.classList.remove('selected');
      loginEl.classList.add('selected');
      this._router.navigate(['/home/login']);

    }else if(loginEl.classList.contains('selected')){
      loginEl.classList.remove('selected');
      registerEl.classList.add('selected');
      this._router.navigate(['/home/register']);

    }

   
  }
}
