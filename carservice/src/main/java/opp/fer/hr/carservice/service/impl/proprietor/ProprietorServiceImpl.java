package opp.fer.hr.carservice.service.impl.proprietor;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.dao.ProprietorRepository;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.exception.PersonAlreadyExistsException;
import opp.fer.hr.carservice.rest.proprietor.CreateProprietorRequest;
import opp.fer.hr.carservice.service.proprietor.ProprietorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ProprietorServiceImpl implements ProprietorService {

    private final ProprietorRepository proprietorRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public ProprietorServiceImpl(ProprietorRepository proprietorRepository, ModelMapper modelMapper) {
        this.proprietorRepository = proprietorRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional // osigurava da se cijela metoda odradi unutar jedne sjednice
    public BasicResponse createProprietor(CreateProprietorRequest createProprietorRequest) {
        if(proprietorRepository.existsByEmail(createProprietorRequest.getEmail())) {
            throw new PersonAlreadyExistsException("Dragi korisniče, email je već iskorišten!");
        }
        if(proprietorRepository.existsByOib(createProprietorRequest.getOib())){
            throw new PersonAlreadyExistsException("Dragi korisniče, OIB je već iskorišten!");
        }
        ProprietorOfVehicle newProprietor = modelMapper.map(createProprietorRequest, ProprietorOfVehicle.class);
        ProprietorOfVehicle savedProprietor = proprietorRepository.save(newProprietor);
        return new BasicResponse(savedProprietor.getId());
    }

<<<<<<< HEAD
=======
    @Override
    public boolean existsByEmail(String username){
        return proprietorRepository.existsByEmail(username);
    }
>>>>>>> backend_users

    @Override
    @Transactional(readOnly = true)
    public ProprietorOfVehicle getProprietor(Long id) {
        Optional<ProprietorOfVehicle> optionalProprietor = proprietorRepository.findById(id);
        if (!optionalProprietor.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, korisnik sa " + id + " id nije pronađen!");
        }
        return optionalProprietor.get();
    }
<<<<<<< HEAD
=======

    @Override
    public Optional<ProprietorOfVehicle> getProprietorOfVehicleByEmail(String email){
        return proprietorRepository.getProprietorOfVehicleByEmail(email);
    }
>>>>>>> backend_users
}
