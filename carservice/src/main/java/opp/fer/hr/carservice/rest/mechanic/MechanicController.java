package opp.fer.hr.carservice.rest.mechanic;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("mechanic")
public class MechanicController {
    private final MechanicService mechanicService;

    @Autowired
    public MechanicController(MechanicService mechanicService){
        this.mechanicService = mechanicService;
    }

    //Autorizacija zahtjeva - ROLE_ADMIN ONLY
    @PostMapping("")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<BasicResponse> createUser(@RequestBody @Valid CreateMechanicRequest createMechanicRequest) {
        return ResponseEntity.ok(mechanicService.createMechanic(createMechanicRequest));
    }

    @GetMapping
    public ResponseEntity<Mechanic> getMechanic(@RequestParam Long id) {
        return ResponseEntity.ok(mechanicService.getMechanic(id));
    }
}
