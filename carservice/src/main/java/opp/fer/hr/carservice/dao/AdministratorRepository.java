package opp.fer.hr.carservice.dao;

import opp.fer.hr.carservice.domain.users.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdministratorRepository extends JpaRepository<Administrator, Long> {
    boolean existsByEmail(String email);
    boolean existsByOib(String oib);
    boolean existsByOibOfService(String oibOfService);
    Optional<Administrator> getAdministratorByEmail(String email);

}
