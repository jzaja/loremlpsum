package opp.fer.hr.carservice.dao;

import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import org.springframework.data.jpa.repository.JpaRepository;

<<<<<<< HEAD
public interface ProprietorRepository extends JpaRepository<ProprietorOfVehicle, Long> {
    boolean existsByEmail(String email);
    boolean existsByOib(String oib);
=======
import java.util.Optional;

public interface ProprietorRepository extends JpaRepository<ProprietorOfVehicle, Long> {
    boolean existsByEmail(String email);
    boolean existsByOib(String oib);
    Optional<ProprietorOfVehicle> getProprietorOfVehicleByEmail(String email);
>>>>>>> backend_users
}
