package opp.fer.hr.carservice.dao;

import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CarServiceRepository extends JpaRepository<CarService, Long> {
    boolean existsByNameOfService(String name);
    boolean existsByAddressOfService(String name);
    boolean existsByAdministrator(Administrator administrator);
    boolean existsById(Long id);
    Optional<CarService> findById(Long id);

    @Query("SELECT g FROM CARSERVICE g WHERE :s MEMBER OF g.mechanics")
    Optional<CarService> findByMechanic(@Param("s") Mechanic mechanic);

}
