package opp.fer.hr.carservice.domain.users;

import org.dom4j.rule.Rule;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * An ordinary person.
 * Person is uniquely identified by internal system ID (a Long)
 * or with the OIB (a string of 11 decimal digits).
 * Email is not necessary.
 * @author Josip Žaja josip.zaja@fer.hr
 */

@MappedSuperclass
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique=true, nullable = false)
    @NotNull
    @Size(min=11, max=11)
    private String oib;

    @Pattern(regexp = "\\w{3,20}")
    @NotNull
    private String givenName;

    @Pattern(regexp = "\\w{3,20}")
    @NotNull
    private String familyName;

    @Email
    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
