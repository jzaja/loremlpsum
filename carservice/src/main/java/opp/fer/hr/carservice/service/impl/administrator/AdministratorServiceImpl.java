package opp.fer.hr.carservice.service.impl.administrator;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.dao.AdministratorRepository;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.exception.PersonAlreadyExistsException;
import opp.fer.hr.carservice.rest.administrator.CreateAdministratorRequest;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AdministratorServiceImpl implements AdministratorService {
    private final AdministratorRepository administratorRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public AdministratorServiceImpl(AdministratorRepository administratorRepository, ModelMapper modelMapper) {
        this.administratorRepository = administratorRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional // osigurava da se cijela metoda odradi unutar jedne sjednice
    public BasicResponse createAdministrator(CreateAdministratorRequest createAdministratorRequest) {
        if(administratorRepository.existsByEmail(createAdministratorRequest.getEmail())) {
            throw new PersonAlreadyExistsException("Dragi korisniče, email je već iskorišten!");
        }
        if(administratorRepository.existsByOib(createAdministratorRequest.getOib())){
            throw new PersonAlreadyExistsException("Dragi korisniče, OIB je već iskorišten!");
        }
        Administrator newAdministrator = modelMapper.map(createAdministratorRequest, Administrator.class);
        Administrator savedAdministrator = administratorRepository.save(newAdministrator);
        return new BasicResponse(savedAdministrator.getId());
    }

    @Override
    public boolean existsByOibOfService(String oibOfService){
        return administratorRepository.existsByOibOfService(oibOfService);
    }

    @Override
    @Transactional(readOnly = true)
    public Administrator getAdministrator(Long id) {
        Optional<Administrator> optionalAdministrator = administratorRepository.findById(id);
        if (!optionalAdministrator.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, korisnik sa " + id + " id nije pronađen!");
        }
        return optionalAdministrator.get();
    }

    @Override
    public boolean existsByEmail(String email){
        if(administratorRepository.existsByEmail(email)){
            return true;
        } else{
            return false;
        }
    }

    @Override
    public Optional<Administrator> getAdministratorByEmail(String email){
        return administratorRepository.getAdministratorByEmail(email);
    }
}
