package opp.fer.hr.carservice.service.impl.carService;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.dao.CarServiceRepository;
import opp.fer.hr.carservice.dao.MechanicRepository;
import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.exception.ServiceAlreadyExistsException;
import opp.fer.hr.carservice.rest.carService.CreateCarServiceRequest;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import opp.fer.hr.carservice.service.carService.CarServiceService;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.OneToMany;
import java.util.List;
import java.util.Optional;

@Service
public class CarServiceServiceImpl implements CarServiceService {

    private final CarServiceRepository carServiceRepository;
    private final ModelMapper modelMapper;
    private final AdministratorService administratorService;
    private final MechanicRepository mechanicRepository;


    @Autowired
    public CarServiceServiceImpl(CarServiceRepository carServiceRepository, ModelMapper modelMapper, AdministratorService administratorService,
                                 MechanicRepository mechanicRepository){
        this.carServiceRepository = carServiceRepository;
        this.modelMapper=modelMapper;
        this.administratorService=administratorService;
        this.mechanicRepository = mechanicRepository;
    }

    public boolean existsByAdministrator(Administrator administrator){
        return carServiceRepository.existsByAdministrator(administrator);
    }


    @Transactional
    @Override
    public BasicResponse createCarService(CreateCarServiceRequest createCarServiceRequest, String username) {
        if(carServiceRepository.existsByNameOfService(createCarServiceRequest.getNameOfService())) {
            throw new ServiceAlreadyExistsException("Dragi korisniče, ime servisa je već iskorišteno!");
        }
        if(carServiceRepository.existsByAddressOfService(createCarServiceRequest.getAddressOfService())){
            throw new ServiceAlreadyExistsException("Dragi korisniče, postoji već servis na toj adresi!");
        }
        if(carServiceRepository.existsByAdministrator(administratorService.getAdministratorByEmail(username).get())){
            throw new ServiceAlreadyExistsException("Dragi korisniče, administrator je već prijavio jedan servis.");
        }
        CarService newCarService = modelMapper.map(createCarServiceRequest, CarService.class);
        newCarService.setAdministrator(administratorService.getAdministratorByEmail(username).get());
        CarService savedCarService = carServiceRepository.save(newCarService);
        return new BasicResponse(savedCarService.getId());
    }

    @Transactional
    @Override
    public List<CarService> listAll(){
        return carServiceRepository.findAll();
    }

    @Override
    public boolean existsById(Long id){
        return carServiceRepository.existsById(id);
    }

    @Override
    public Optional<CarService> findById(Long id){
        return carServiceRepository.findById(id);
    }


    @Override
    public String addMechanic(Long carServiceId, Long mechanicId) {
        Optional<CarService> carService = carServiceRepository.findById(carServiceId);
        if(!carService.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, autoservis sa takvim ID-om ne postoji!");
        }
        CarService carService1 = carService.get();
        Optional<Mechanic> mechanic = mechanicRepository.findById(mechanicId);
        if(!mechanic.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, serviser sa takvim ID-om ne postoji!");
        }
        Mechanic mechanic1 = mechanic.get();
        carService1.addMechanic(mechanic1);
        return "Uspješno ste izvršili priduživanje servisera servisu!";
    }

}
