package opp.fer.hr.carservice.service.carService;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.rest.carService.CreateCarServiceRequest;

import java.util.List;
import java.util.Optional;


public interface CarServiceService {
    BasicResponse createCarService(CreateCarServiceRequest createCarServiceRequest, String username);
    List<CarService> listAll();
    boolean existsByAdministrator(Administrator administrator);
    String addMechanic(Long carServiceId, Long mechanicId);
    boolean existsById(Long id);
    Optional<CarService> findById(Long id);
}
