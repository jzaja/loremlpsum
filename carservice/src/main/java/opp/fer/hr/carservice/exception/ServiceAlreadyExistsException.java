package opp.fer.hr.carservice.exception;

public class ServiceAlreadyExistsException extends RuntimeException {
    public ServiceAlreadyExistsException(String message){
        super(message);
    }
}
