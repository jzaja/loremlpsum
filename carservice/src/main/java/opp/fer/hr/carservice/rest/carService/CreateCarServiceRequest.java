package opp.fer.hr.carservice.rest.carService;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateCarServiceRequest {
    @Pattern(regexp = "\\w{3,20} \\d{1,2}", message = "Dragi korisniče, adresa servisa nije u ispravnom formatu!")
    private String addressOfService;

    private String nameOfService;

    private Double price;

    @Size(min=11, max=11, message = "Dragi korisniče, OIB nije u ispravnom formatu (različit od 11 znamenki)!")
    private String oibOfService;

    public String getAddressOfService() {
        return addressOfService;
    }

    public void setAddressOfService(String addressOfService) {
        this.addressOfService = addressOfService;
    }

    public String getNameOfService() {
        return nameOfService;
    }

    public void setNameOfService(String nameOfService) {
        this.nameOfService = nameOfService;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getOibOfService() {
        return oibOfService;
    }

    public void setOibOfService(String oibOfService) {
        this.oibOfService = oibOfService;
    }
}
