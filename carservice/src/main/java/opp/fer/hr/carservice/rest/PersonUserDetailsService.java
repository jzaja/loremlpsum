package opp.fer.hr.carservice.rest;

import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
<<<<<<< HEAD
=======
import opp.fer.hr.carservice.service.mechanic.MechanicService;
>>>>>>> backend_users
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

/**
 * Specijalna vrsta Servisa koja će biti opet u rest paketu, rekli smo sve što ima veze sa
 * autorizacijom i autentifikacijom ide u rest paket
 */
@Service
public class PersonUserDetailsService implements UserDetailsService {

    private final AdministratorService administratorService;


    @Autowired
    public PersonUserDetailsService(AdministratorService administratorService){
        this.administratorService = administratorService;
    }

    public String getPass(String email){
        Administrator administrator = administratorService.getAdministratorByEmail(email).get();
        return administrator.getPassword();
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (administratorService.existsByEmail(username))
            return new User(
                    username,
                    getPass(username),
                    commaSeparatedStringToAuthorityList("ROLE_ADMIN")
            );
        else throw new UsernameNotFoundException("No user " + username);
    }
}
