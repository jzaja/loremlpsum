package opp.fer.hr.carservice.service.proprietor;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.rest.proprietor.CreateProprietorRequest;

<<<<<<< HEAD
public interface ProprietorService {
    public BasicResponse createProprietor(CreateProprietorRequest createProprietorRequest);
    public ProprietorOfVehicle getProprietor(Long id);
=======
import java.util.Optional;

public interface ProprietorService {
    public BasicResponse createProprietor(CreateProprietorRequest createProprietorRequest);
    public ProprietorOfVehicle getProprietor(Long id);
    boolean existsByEmail(String email);
    Optional<ProprietorOfVehicle> getProprietorOfVehicleByEmail(String email);
>>>>>>> backend_users
}
