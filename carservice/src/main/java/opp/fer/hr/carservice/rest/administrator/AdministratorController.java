package opp.fer.hr.carservice.rest.administrator;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("administrator")
public class AdministratorController {
    private final AdministratorService administratorService;

    @Autowired
    public AdministratorController(AdministratorService administratorService){
        this.administratorService = administratorService;
    }

    @PostMapping("")
    public ResponseEntity<BasicResponse> createUser(@RequestBody @Valid CreateAdministratorRequest createAdministratorRequest) {
        return ResponseEntity.ok(administratorService.createAdministrator(createAdministratorRequest));
    }

    @GetMapping
    public ResponseEntity<Administrator> getAdministrator(@RequestParam Long id) {
        return ResponseEntity.ok(administratorService.getAdministrator(id));
    }
}

