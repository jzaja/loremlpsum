package opp.fer.hr.carservice.domain.carService;

import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "CARSERVICE")
public class CarService {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String nameOfService;

    @Column(unique = true, nullable = false)
    private String addressOfService;

    @OneToOne
    @JoinColumn(name = "oibOfService")
    private Administrator administrator;

    @OneToMany
    private Set<Mechanic> mechanics;

    @Column(nullable = false)
    private Double price;

    public Set<Mechanic> getMechanics() {
        return mechanics;
    }
    public void setMechanics(Set<Mechanic> mechanics) {
        this.mechanics = mechanics;
    }

    public void addMechanic(Mechanic mechanic){
        mechanics.add(mechanic);
    }

    public Long getId() {
        return id;
    }

    public String getNameOfService() {
        return nameOfService;
    }

    public void setNameOfService(String nameOfService) {
        this.nameOfService = nameOfService;
    }

    public String getAddressOfService() {
        return addressOfService;
    }

    public void setAddressOfService(String addressOfService) {
        this.addressOfService = addressOfService;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
