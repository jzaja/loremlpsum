package opp.fer.hr.carservice.dao;

import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProprietorRepository extends JpaRepository<ProprietorOfVehicle, Long> {
    boolean existsByEmail(String email);
    boolean existsByOib(String oib);
    Optional<ProprietorOfVehicle> getProprietorOfVehiclesByEmail(String email);
}
