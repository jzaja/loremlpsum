package opp.fer.hr.carservice.service.impl.vehicle;

import java.util.List;
import java.util.Optional;

import opp.fer.hr.carservice.dao.ProprietorRepository;
import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import opp.fer.hr.carservice.core.VehicleBasicResponse;
import opp.fer.hr.carservice.dao.VehicleRepository;
import opp.fer.hr.carservice.domain.vehicle.Vehicle;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.rest.vehicle.CreateVehicleRequest;
import opp.fer.hr.carservice.service.vehicle.VehicleService;
import fer.opp.project.carservice.vehicle.exceptions.VehicleAlreadyExistsException;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository, ModelMapper modelMapper) {
        this.vehicleRepository = vehicleRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional // osigurava da se cijela metoda odradi unutar jedne sjednice
    public VehicleBasicResponse createVehicle(CreateVehicleRequest createVehicleRequest) {
        if (vehicleRepository.existsByLicensePlate(createVehicleRequest.getLicensePlate())) {
            throw new VehicleAlreadyExistsException("Vozilo s tim registracijskim oznakama je vec upisano!");
        }

        Vehicle newVehicle = modelMapper.map(createVehicleRequest, Vehicle.class);
        Vehicle savedVehicle = vehicleRepository.save(newVehicle);
        return new VehicleBasicResponse(savedVehicle.getLicensePlate());
    }

    @Override
    @Transactional(readOnly = true)
    public Vehicle getVehicle(String licensePlate){
        Optional<Vehicle> optionalVehicle = vehicleRepository.findById(licensePlate);
        if (!optionalVehicle.isPresent()) {
            throw new EntityNotFoundException("Vozilo sa " + licensePlate + " registracijskim oznakama nije pronađen!");
        }
        return optionalVehicle.get();
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> getVehiclesByOwner (String oib){
    	List<Vehicle> vehicleList = vehicleRepository.getVehiclesByOib(oib);
    	if(vehicleList.isEmpty()) {
    		throw new EntityNotFoundException("Osoba s oibom " + oib + " nema upisanih vozila!");
    	}
    	return vehicleList;
    }

    @Override
    public boolean removeVehicle(String licensePlate) {
    	Optional<Vehicle> optionalVehicle = vehicleRepository.findById(licensePlate);
    	if (!optionalVehicle.isPresent()) {
            throw new EntityNotFoundException("Vozilo sa " + licensePlate + " registracijskim oznakama nije pronađen!");
        }
    
    	vehicleRepository.deleteById(licensePlate);
    	return true;
    }

    /*@Override
    public String addMechanic(String proprietorEmail, Long vehicleId) {
        Optional<ProprietorOfVehicle> proprietorOfVehicle = ProprietorRepository.getProprietorOfVehicleByEmail(proprietorEmail);
        if(!proprietorOfVehicle.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, autoservis sa takvim ID-om ne postoji!");
        }
        CarService carService1 = carService.get();
        Optional<Mechanic> mechanic = mechanicRepository.findById(mechanicId);
        if(!mechanic.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, serviser sa takvim ID-om ne postoji!");
        }
        Mechanic mechanic1 = mechanic.get();
        carService1.addMechanic(mechanic1);
        carServiceRepository.save(carService1);
        return "Uspješno ste izvršili priduživanje servisera servisu!";
    }*/
}
