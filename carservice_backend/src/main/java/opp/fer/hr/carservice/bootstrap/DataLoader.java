package opp.fer.hr.carservice.bootstrap;

import opp.fer.hr.carservice.rest.administrator.CreateAdministratorRequest;
import opp.fer.hr.carservice.rest.carService.CreateCarServiceRequest;
import opp.fer.hr.carservice.rest.mechanic.CreateMechanicRequest;
import opp.fer.hr.carservice.rest.proprietor.CreateProprietorRequest;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import opp.fer.hr.carservice.service.carService.CarServiceService;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import opp.fer.hr.carservice.service.proprietor.ProprietorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final AdministratorService administratorService;
    private final ProprietorService proprietorService;
    private final MechanicService mechanicService;
    private final CarServiceService carServiceService;

    @Autowired
    public DataLoader(AdministratorService administratorService, ProprietorService proprietorService, MechanicService mechanicService, CarServiceService carServiceService){
        this.administratorService = administratorService;
        this.proprietorService = proprietorService;
        this.mechanicService = mechanicService;
        this.carServiceService = carServiceService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent){
        try {
            initData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() throws Exception {
        CreateProprietorRequest createProprietorRequest = new CreateProprietorRequest();
        createProprietorRequest.setEmail("josip_zaja@gmail.com");
        createProprietorRequest.setFamilyName("Zaja");
        createProprietorRequest.setGivenName("Josip");
        createProprietorRequest.setOib("12345678910");
        createProprietorRequest.setPassword("pass");
        proprietorService.createProprietor(createProprietorRequest);

        CreateAdministratorRequest createAdministratorRequest = new CreateAdministratorRequest();
        createAdministratorRequest.setEmail("toma_petar@hotmail.com");
        createAdministratorRequest.setOib("10987654321");
        createAdministratorRequest.setGivenName("Toma");
        createAdministratorRequest.setFamilyName("Sikora");
        createAdministratorRequest.setPassword("trlababalan");
        createAdministratorRequest.setOibOfService("00000000000");
        administratorService.createAdministrator(createAdministratorRequest);


        CreateAdministratorRequest createAdministratorRequest1=new CreateAdministratorRequest();
        createAdministratorRequest1.setEmail("bruno_skendrovic@gmail.com");
        createAdministratorRequest1.setOibOfService("99999999999");
        createAdministratorRequest1.setOib("12121212121");
        createAdministratorRequest1.setFamilyName("Skendrovic");
        createAdministratorRequest1.setGivenName("Bruno");
        createAdministratorRequest1.setPassword("ivana");
        administratorService.createAdministrator(createAdministratorRequest1);

        CreateCarServiceRequest createCarServiceRequest = new CreateCarServiceRequest();
        createCarServiceRequest.setAddressOfService("Marasoviceva 57");
        createCarServiceRequest.setNameOfService("ServisMaster");
        createCarServiceRequest.setOibOfService("00000000000");
        createCarServiceRequest.setPrice(1234.0);
        carServiceService.createCarService(createCarServiceRequest, "toma_petar@hotmail.com");


        CreateMechanicRequest createMechanicRequest = new CreateMechanicRequest();
        createMechanicRequest.setEmail("bruno_juric@gmail.com");
        createMechanicRequest.setFamilyName("Juric");
        createMechanicRequest.setGivenName("Bruno");
        createMechanicRequest.setOib("12345543210");
        createMechanicRequest.setOibOfService("00000000000");
        createMechanicRequest.setPassword("mojajedina");
        mechanicService.createMechanic(createMechanicRequest);

    }

}
