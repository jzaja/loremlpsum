package opp.fer.hr.carservice.service.impl.mechanic;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.dao.MechanicRepository;
import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.exception.PersonAlreadyExistsException;
import opp.fer.hr.carservice.rest.mechanic.CreateMechanicRequest;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class MechanicServiceImpl implements MechanicService {

    private final MechanicRepository mechanicRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public MechanicServiceImpl(MechanicRepository mechanicRepository, ModelMapper modelMapper) {
        this.mechanicRepository = mechanicRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional // osigurava da se cijela metoda odradi unutar jedne sjednice
    public BasicResponse createMechanic(CreateMechanicRequest createUserRequest) {
        if(mechanicRepository.existsByEmail(createUserRequest.getEmail())) {
            throw new PersonAlreadyExistsException("Dragi korisniče, email je već iskorišten!");
        }
        if(mechanicRepository.existsByOib(createUserRequest.getOib())){
            throw new PersonAlreadyExistsException("Dragi korisniče, OIB je već iskorišten!");
        }
        Mechanic newMechanic = modelMapper.map(createUserRequest, Mechanic.class);
        Mechanic savedMechanic = mechanicRepository.save(newMechanic);
        return new BasicResponse(savedMechanic.getId());
    }

    @Override
    public Mechanic fetch(Long mechanicId) {
        return findById(mechanicId).orElseThrow(
                () -> new EntityNotFoundException(mechanicRepository.findById(mechanicId).toString())
        );
    }

    public boolean existsByEmail(String email){
        return mechanicRepository.existsByEmail(email);
    }

    @Override
    public Optional<Mechanic> findById(Long mechanicId) {
        return mechanicRepository.findById(mechanicId);
    }

    @Override
    @Transactional(readOnly = true)
    public Mechanic getMechanic(Long id) {
        Optional<Mechanic> optionalMechanic = mechanicRepository.findById(id);
        if (!optionalMechanic.isPresent()) {
            throw new EntityNotFoundException("Dragi korisniče, korisnik sa " + id + " id nije pronađen!");
        }
        return optionalMechanic.get();
    }

    @Override
    public Optional<Mechanic> getMechanicByEmail(String email){
        return mechanicRepository.getMechanicByEmail(email);
    }
}
