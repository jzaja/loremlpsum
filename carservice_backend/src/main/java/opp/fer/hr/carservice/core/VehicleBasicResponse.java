package opp.fer.hr.carservice.core;

public class VehicleBasicResponse {

    private String licensePlate;

    public VehicleBasicResponse(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public static class VehicleBooleanResponse {
    }
}
