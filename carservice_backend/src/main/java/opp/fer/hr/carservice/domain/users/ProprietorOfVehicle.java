package opp.fer.hr.carservice.domain.users;

import opp.fer.hr.carservice.domain.users.Person;
import opp.fer.hr.carservice.domain.vehicle.Vehicle;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class ProprietorOfVehicle extends Person {
    @OneToMany
    private Set<Vehicle> vehicles;

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }
    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public void addVehicle(Vehicle vehicle){
        vehicles.add(vehicle);
        System.out.println(vehicles.size());
    }
}
