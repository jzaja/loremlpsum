package opp.fer.hr.carservice.rest.carService;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import opp.fer.hr.carservice.service.carService.CarServiceService;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import org.hibernate.action.internal.EntityActionVetoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("carservice")
public class CarServiceController {

    private final CarServiceService carServiceService;
    private final AdministratorService administratorService;
    private final MechanicService mechanicService;

    @Autowired
    public CarServiceController(CarServiceService carServiceService, AdministratorService administratorService, MechanicService mechanicService) {
        this.carServiceService = carServiceService;
        this.administratorService = administratorService;
        this.mechanicService = mechanicService;
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("")
    public ResponseEntity<BasicResponse> createUser(@RequestBody @Valid CreateCarServiceRequest createCarServiceRequest, @AuthenticationPrincipal User u) {
        return ResponseEntity.ok(carServiceService.createCarService(createCarServiceRequest, u.getUsername()));
    }

    @GetMapping("")
    public List<CarService> listStudents() {
        return carServiceService.listAll();
    }



    @PutMapping("/{oibCarService}/mechanics/{mechanicEmail}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> addGroupMember(
            @PathVariable("oibCarService") String oibCarService,
            @PathVariable("mechanicEmail") String mechanicEmail,
            @AuthenticationPrincipal User u) {
    	Optional<Administrator> admin = administratorService.findByOibOfService(oibCarService);
    	Optional<CarService> carService = carServiceService.findByAdministrator(admin.get());
    	Mechanic mechanic = mechanicService.getMechanicByEmail(mechanicEmail).get();
        return ResponseEntity.ok(carServiceService.addMechanic(carService.get().getId(), mechanic.getId()));
    }

    private void checkPermissionToUpdateCarService(Long carServiceId, Long mechanicId, String email) {
        Optional<Administrator> administrator = administratorService.getAdministratorByEmail(email);
        if(administrator.isEmpty()) {
            throw new EntityNotFoundException("Dragi korisniče, administrator sa navedenim emailom ne postoji");
        }
        Administrator administrator1 = administrator.get();
        if(!carServiceService.existsById(carServiceId)){
            throw new EntityNotFoundException("Dragi korisniče, servis sa navedenim ID-om ne postoji!");
        }
        Optional<Mechanic> mechanic = mechanicService.findById(mechanicId);
        if(mechanic.isEmpty()) {
            throw new EntityNotFoundException("Dragi korisniče, serviser sa navedenim ID-om ne postoji");
        }
        if(!administrator1.getEmail().equals(email)) {
            throw new AccessDeniedException("Dragi korisniče, nemate ovlasti za navedenu radnju!");
        }
        if(!administrator1.getOibOfService().equals(mechanic.get().getOibOfService())) {
            throw new AccessDeniedException("Dragi korisniče, nemate ovlasti..");
        }
    }
}
