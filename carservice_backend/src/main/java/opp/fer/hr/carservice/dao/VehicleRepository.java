package opp.fer.hr.carservice.dao;

import java.util.List;
import java.util.Optional;

import opp.fer.hr.carservice.domain.carService.CarService;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.domain.vehicle.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, String>{

    boolean existsByLicensePlate(String licensePlate);
    
    List<Vehicle> getVehiclesByOib(String oib);

}
