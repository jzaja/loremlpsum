package opp.fer.hr.carservice.service.vehicle;


import java.util.List;

import opp.fer.hr.carservice.core.VehicleBasicResponse;
import opp.fer.hr.carservice.domain.vehicle.Vehicle;
import opp.fer.hr.carservice.rest.vehicle.CreateVehicleRequest;

public interface VehicleService {

    public VehicleBasicResponse createVehicle(CreateVehicleRequest createVehicleRequest); // unos novih vozila

    public Vehicle getVehicle(String licensePlate);  // dohvacanje vozila po registracijskoj oznaci
    
    public List<Vehicle> getVehiclesByOwner(String oib); // dohvacanje vozila po oibu vlasnika vozila

    public boolean removeVehicle(String licensePlate); // brisane vozila

    //public String addMechanic(Long carServiceId, Long mechanicId);
}
