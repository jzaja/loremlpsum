package opp.fer.hr.carservice.rest.log;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.exception.EntityNotFoundException;
import opp.fer.hr.carservice.rest.proprietor.CreateProprietorRequest;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import opp.fer.hr.carservice.service.proprietor.ProprietorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("login")
public class LogController {

    private final AdministratorService administratorService;
    private final MechanicService mechanicService;
    private final ProprietorService proprietorService;
    @Autowired
    private final PasswordEncoder bcpe;


    @Autowired
    public LogController(AdministratorService administratorService, MechanicService mechanicService, ProprietorService proprietorService,
                         PasswordEncoder bcpe){
        this.administratorService = administratorService;
        this.mechanicService = mechanicService;
        this.proprietorService = proprietorService;
        this.bcpe = bcpe;
    }

    public String bcrypt(String pass) {
        String pw_hash = BCrypt.hashpw(pass, BCrypt.gensalt(10));
        return pw_hash;
    }


    public String base64hash(CreateLogRequest createLogRequest){
        Base64.Encoder encoder = Base64.getEncoder();
        String username = createLogRequest.getEmail();
        String password = createLogRequest.getPass();
        String normalString = new String(username + ":" + password);
        String encodedString = encoder.encodeToString(
                normalString.getBytes(StandardCharsets.UTF_8));
        return  encodedString;
    }
    
    @PostMapping("")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Map<String,String>> loginUser(@RequestBody CreateLogRequest createLogRequest) {
    	Map<String,String> Res=new HashMap<String, String>();
        if(administratorService.existsByEmail(createLogRequest.getEmail())) {
            Administrator administrator = administratorService.getAdministratorByEmail(createLogRequest.getEmail()).get();
            if(bcpe.matches(createLogRequest.getPass(), administrator.getPassword())){
            	Res.put("token", base64hash(createLogRequest));
            	Res.put("role", "admin");
                return ResponseEntity.ok(Res);
            }
        }
        if(mechanicService.existsByEmail(createLogRequest.getEmail())) {
            Mechanic mechanic = mechanicService.getMechanicByEmail(createLogRequest.getEmail()).get();
            if(bcpe.matches(createLogRequest.getPass(), mechanic.getPassword())){
            	Res.put("token", base64hash(createLogRequest));
            	Res.put("role", "mechanic");
                return ResponseEntity.ok(Res);
            }
        }
        if(proprietorService.existsByEmail(createLogRequest.getEmail())) {
            ProprietorOfVehicle proprietorOfVehicle = proprietorService.getProprietorOfVehiclesByEmail(createLogRequest.getEmail()).get();
            if(bcpe.matches(createLogRequest.getPass(), proprietorOfVehicle.getPassword())) {
            	Res.put("token", base64hash(createLogRequest));
            	Res.put("role", "user");
            	return ResponseEntity.ok(Res);
                
            }
        }
        Res.put("error","Dragi korisniče email i sifra se ne podudaraju");
        return ResponseEntity.ok(Res);
    }
}
