package opp.fer.hr.carservice.rest.proprietor;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.service.proprietor.ProprietorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("proprietor")
public class ProprietorOfVehicleController {
    private final ProprietorService proprietorService;

    @Autowired
    public ProprietorOfVehicleController(ProprietorService proprietorService){
        this.proprietorService = proprietorService;
    }


    @PostMapping("")
    @CrossOrigin(origins = "*")
    public ResponseEntity<BasicResponse> createUser(@RequestBody @Valid CreateProprietorRequest createProprietorRequest) {
        return ResponseEntity.ok(proprietorService.createProprietor(createProprietorRequest));
    }

    @GetMapping
    @CrossOrigin(origins = "*")
    public ResponseEntity<ProprietorOfVehicle> getMechanic(@RequestParam Long id) {
        return ResponseEntity.ok(proprietorService.getProprietor(id));
    }

    @GetMapping("/info")
    @CrossOrigin(origins = "*")
    @Secured("ROLE_PROPRIETOR")
    public ResponseEntity<?> getUserByEmail( @AuthenticationPrincipal User u) {
        return ResponseEntity.ok(proprietorService.getProprietorOfVehiclesByEmail(u.getUsername()));
    }
}
