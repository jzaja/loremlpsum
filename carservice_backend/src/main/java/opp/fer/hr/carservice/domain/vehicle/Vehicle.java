package opp.fer.hr.carservice.domain.vehicle;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

//import fer.opp.project.carservice.ticket.Ticket;

@Entity(name = "Vehicle")
public class Vehicle {

    @Id
    @Column(unique = true, updatable = false)
    private String licensePlate;
    private String carBrand;
    private String oib;
    private Short  hasMalfunction;
    private Date serviceDate;
    //@OneToMany(mappedBy = "licensePlate", cascade = CascadeType.ALL)
    //private Set<Ticket> tickets;

    
    

	public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public Short getHasMalfunction() {
        return hasMalfunction;
    }

    public void setHasMalfunction(Short hasMalfunction) {
        this.hasMalfunction = hasMalfunction;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }
}
