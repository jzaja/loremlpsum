package opp.fer.hr.carservice.rest.vehicle;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

public class CreateVehicleRequest {

    @NotNull
    @Pattern(regexp = "\\w{2}-\\d{3,4}-\\w{1,2}", message = "Dragi korisniče, tablica nije u ispravnom formatu!")
    private String licensePlate;

    private String carBrand;

    @Size(min = 11, max = 11)
    private String OIB;

    private Short hasMalfunction;

    private Date serviceDate;


    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getOIB() {
        return OIB;
    }

    public void setOIB(String OIB) {
        this.OIB = OIB;
    }

    public Short getHasMalfunction() {
        return hasMalfunction;
    }

    public void setHasMalfunction(Short hasMalfunction) {
        this.hasMalfunction = hasMalfunction;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }


}
