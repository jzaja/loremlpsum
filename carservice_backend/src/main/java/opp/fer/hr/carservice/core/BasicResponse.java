package opp.fer.hr.carservice.core;

import org.springframework.http.converter.json.GsonBuilderUtils;

public class BasicResponse {

    private Long id;

    public BasicResponse(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
