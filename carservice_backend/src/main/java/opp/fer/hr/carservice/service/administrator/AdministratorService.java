package opp.fer.hr.carservice.service.administrator;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.rest.administrator.CreateAdministratorRequest;

import java.util.Optional;

public interface AdministratorService {
    public BasicResponse createAdministrator(CreateAdministratorRequest createAdministratorRequest);
    public Administrator getAdministrator(Long id);
    public boolean existsByEmail(String email);
    public Optional<Administrator> getAdministratorByEmail(String email);
    public boolean existsByOibOfService(String oibOfService);
    Optional<Administrator> findByOibOfService(String oibOfService);
}
