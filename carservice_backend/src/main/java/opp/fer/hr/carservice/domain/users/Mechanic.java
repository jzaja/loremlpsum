package opp.fer.hr.carservice.domain.users;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Mechanic extends Person {

    @Column(unique = true)
    @Size(min = 11, max = 11)
    private String oibOfService;

    public String getOibOfService() {
        return oibOfService;
    }

    public void setOibOfService(String oibOfService) {
        this.oibOfService = oibOfService;
    }
}
