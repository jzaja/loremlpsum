package opp.fer.hr.carservice.rest.vehicle;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import opp.fer.hr.carservice.core.VehicleBasicResponse;
import opp.fer.hr.carservice.domain.vehicle.Vehicle;
import opp.fer.hr.carservice.service.vehicle.VehicleService;

@RestController
@RequestMapping("vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("")
    public ResponseEntity<VehicleBasicResponse> createVehicle(@RequestBody @Valid CreateVehicleRequest createVehicleRequest) {
        return ResponseEntity.ok(vehicleService.createVehicle(createVehicleRequest));
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{licensePlate}")
    public ResponseEntity<Vehicle> getVehicle(@PathVariable("licensePlate") String licensePlate) {
        return ResponseEntity.ok(vehicleService.getVehicle(licensePlate));
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/all{oib}")
    public ResponseEntity<List<Vehicle>> getVehiclesByOwner(@PathVariable("oib") String oib){
    	return ResponseEntity.ok(vehicleService.getVehiclesByOwner(oib));
    }
    
    @DeleteMapping("/{licensePlate}")
    public ResponseEntity<?> removeVehicle(@PathVariable("licensePlate") String licensePlate) {
    	return ResponseEntity.ok(vehicleService.removeVehicle(licensePlate));
    }

}
