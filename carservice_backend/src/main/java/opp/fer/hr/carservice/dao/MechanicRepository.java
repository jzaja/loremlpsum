package opp.fer.hr.carservice.dao;

import opp.fer.hr.carservice.domain.users.Mechanic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MechanicRepository extends JpaRepository<Mechanic, Long> {
    boolean existsByEmail(String email);
    boolean existsByOib(String oib);
    Optional<Mechanic> getMechanicByEmail(String email);
}
