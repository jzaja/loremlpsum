package opp.fer.hr.carservice.domain.users;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
public class Administrator extends Person {

    @Column(unique = true, nullable = false)
    @Size(min = 11, max = 11)
    private String oibOfService;

    public String getOibOfService() {
        return oibOfService;
    }

    public void setOibOfService(String oibOfService) {
        this.oibOfService = oibOfService;
    }
}
