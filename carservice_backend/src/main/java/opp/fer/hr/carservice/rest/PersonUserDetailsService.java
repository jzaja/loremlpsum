package opp.fer.hr.carservice.rest;

import opp.fer.hr.carservice.domain.users.Administrator;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.domain.users.ProprietorOfVehicle;
import opp.fer.hr.carservice.service.administrator.AdministratorService;
import opp.fer.hr.carservice.service.mechanic.MechanicService;
import opp.fer.hr.carservice.service.proprietor.ProprietorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

/**
 * Specijalna vrsta Servisa koja će biti opet u rest paketu, rekli smo sve što ima veze sa
 * autorizacijom i autentifikacijom ide u rest paket
 */
@Service
public class PersonUserDetailsService implements UserDetailsService {

    private final AdministratorService administratorService;
    private final ProprietorService proprietorService;
    private final MechanicService mechanicService;


    @Autowired
    public PersonUserDetailsService(AdministratorService administratorService, ProprietorService proprietorService, MechanicService mechanicService){
        this.administratorService = administratorService;
        this.proprietorService=proprietorService;
        this.mechanicService = mechanicService;
    }

    public String getPass1(String email){
        Administrator administrator = administratorService.getAdministratorByEmail(email).get();
        return administrator.getPassword();
    }

    public String getPass2(String email){
        ProprietorOfVehicle proprietorOfVehicle = proprietorService.getProprietorOfVehiclesByEmail(email).get();
        return  proprietorOfVehicle.getPassword();
    }

    public String getPass3(String email){
        Mechanic mechanic = mechanicService.getMechanicByEmail(email).get();
        return  mechanic.getPassword();
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (administratorService.existsByEmail(username))
            return new User(
                    username,
                    getPass1(username),
                    commaSeparatedStringToAuthorityList("ROLE_ADMIN")
            );
       else if(proprietorService.existsByEmail(username))
            return new User(
                    username,
                    getPass2(username),
                    commaSeparatedStringToAuthorityList("ROLE_PROPRIETOR")
            );
        else if(mechanicService.existsByEmail(username))
            return new User(
                    username,
                    getPass3(username),
                    commaSeparatedStringToAuthorityList("ROLE_MECHANIC")
            );
        else throw new UsernameNotFoundException("No user " + username);
    }
}
