package opp.fer.hr.carservice.service.mechanic;

import opp.fer.hr.carservice.core.BasicResponse;
import opp.fer.hr.carservice.domain.users.Mechanic;
import opp.fer.hr.carservice.rest.mechanic.CreateMechanicRequest;

import java.util.Optional;

public interface MechanicService {
    BasicResponse createMechanic(CreateMechanicRequest createMechanicRequest);
    Mechanic getMechanic(Long id);
    Optional<Mechanic> findById(Long mechanicId);
    Mechanic fetch(Long mechanicId);
    boolean existsByEmail(String email);
    Optional<Mechanic> getMechanicByEmail(String email);
}
