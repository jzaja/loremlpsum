package opp.fer.hr.carservice.core;

public class VehicleBooleanResponse {
    private Boolean response;

    public VehicleBooleanResponse(Boolean res) {
        this.response = res;
    }
    public Boolean getLicensePlate() {
        return response;
    }
    public void setLicensePlate(Boolean response) {
        this.response = response;
    }
}
