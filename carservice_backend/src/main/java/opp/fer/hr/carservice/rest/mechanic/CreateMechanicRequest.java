package opp.fer.hr.carservice.rest.mechanic;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateMechanicRequest {
    @Pattern(regexp = "\\w{3,20}", message = "Dragi korisniče, ime nije u ispravnom formatu!")
    private String givenName;

    @Pattern(regexp = "\\w{3,20}", message = "Dragi korisniče, prezime nije u ispravnom formatu!")
    private String familyName;

    @Email(message = "Email nije u ispravnom formatu!")
    private String email;

    @Size(min=11, max=11, message = "Dragi korisniče, OIB nije u ispravnom formatu (različit od 11 znamenki)!")
    private String oib;

    @Size(min=11, max=11, message = "Dragi korisniče, OIB servisera nije u ispravnom formatu (različit od 11 znamenki)!")
    private String oibOfService;

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        String pw_hash = BCrypt.hashpw(password, BCrypt.gensalt(10));
        this.password = pw_hash;
    }


    public String getOibOfService() {
        return oibOfService;
    }

    public void setOibOfService(String oibOfService) {
        this.oibOfService = oibOfService;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }
}
